#!/usr/bin/perl
use strict;
use DBI;

print "Content-type: text/html\n\n";

my $dbfile = "test";
my $host = "172.17.0.5";
my $dsn      = "dbi:mysql:dbname=$dbfile;host=$host";
my $user     = "golden";
my $password = "password";
my $dbh = DBI->connect($dsn, $user, $password, {
   PrintError       => 0,
   RaiseError       => 1,
   AutoCommit       => 1,
   FetchHashKeyName => 'NAME_lc',
});

my $sth = $dbh->prepare( "SELECT * FROM data;" );
$sth->execute();

my $fields = $sth->{NUM_OF_FIELDS};
my $rows = $sth->rows();
print "Selected $rows row(s) with $fields field(s)\n";

for ( 1 .. $rows ) {
  my ($col1, $col2, $col3, $col4) = $sth->fetchrow();
  print "$col1 $col2 $col3 $col4\n";
}

$sth->finish();
$dbh->disconnect();
